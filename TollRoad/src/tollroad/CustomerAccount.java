package tollroad;

/**
 * @author : Jack Warner-Pankhurst
 * Date : 23/02/18
 * Program : Customer account object class
 * Version : 3
 */
public class CustomerAccount implements Comparable<CustomerAccount>{
    private final String firstName; //first name
    private final String lastName; //last name
    private Vehicle vehicle; //vehicle object refernce
    private int credit; //current amount of credit
    private enum DiscountType {NONE,STAFF,FRIENDS_AND_FAMILY};
    private DiscountType discountType;//enum for discount types
        
    public CustomerAccount() //default constructor (Missing assigmnets defualt
    {                        //to NULL already
        this.firstName = "Not found;";
        this.lastName = "Not found.";
        this.credit = -1;
    }
    public CustomerAccount(String firstn, String lastn, Vehicle vehicle,
                            int credit)//Normal constructor
    { 
        this.firstName = firstn;
        this.lastName = lastn;
        this.vehicle = vehicle;
        this.credit = credit;
        this.discountType = DiscountType.NONE; //defaults to none without use of 
    }                                          //setter methods
    public String getFirstName() //returns first name
    {
        return this.firstName;
    }
    public String getLastName() //returns last name
    {
        return this.lastName;
    }
    public Vehicle getVehicle()//returns vehicle object associated with accoount
    {
        return this.vehicle;
    }
    public int getCredit() //returns credit
    {
        return this.credit;
    }
    public void subtractCredit(int credit) //subtracts amount from credit
    {
        this.credit -= credit;
    }
    public void activateStaffDiscount() //activates staff discount
    {
        this.discountType = DiscountType.STAFF;
    }
    public void activateFriends_And_FamilyDiscount()// activates F&F discount
    {
        this.discountType = DiscountType.FRIENDS_AND_FAMILY;
    }
    public void deactivateDiscount() //sets discount to NONE (no discount)
    {
        this.discountType = DiscountType.NONE;
    }
    public void addFunds(int funds) //adds funds
    {
        this.credit += funds;
    }
    public int makeTrip() //simulates making a trip with accounts vehicle    
    {
        try {           
            int cost = this.vehicle.calculateBasicTrip(); 
            //gets the undiscounted cost of the trip based on account vehicle 

            switch (this.discountType) //calcaulates cost based on dicount type
            { //casting values to int so they are correctly rounded
                case NONE : 
                    break;
                case STAFF : 
                    cost = (int) (cost/2);
                    break;
                    
                case FRIENDS_AND_FAMILY :
                    cost = (int) (cost*0.9);
                    break;
            }            
            if (this.credit - cost >= 0)//if customer has enough credit to pay
            {
                this.subtractCredit(cost); //subtract cost from account credit
                return cost;
            }
  
            else 
            { //otherwise throw exception as account has insufficient credit 
                throw new InsufficientAccountBalanceException();
            }
        }
        catch (InsufficientAccountBalanceException e)
        {
            System.out.println(this.getVehicle().getRegPlate() + 
                    " has insufficient credit. Continuing.");
        }
        return 0; //if something goes wrong, will never return this
    }
    @Override
    public String toString() //returns formatted string representation of object
    {
        return this.firstName +" - "+ this.lastName +" - Credit: "+ this.credit 
               + " - " + this.vehicle.toString();
    }
    @Override
    public int compareTo(CustomerAccount acc) 
    { //compares reg plates of two vehicles, returns 0 if equal, -1 if first
      //reg plate is before second, 1 if seconds plate is before first
      int i = this.vehicle.regPlate.compareTo(acc.vehicle.regPlate);
      if(i <0) return -1;
      else if ( i > 0) return 1;
      else return 0;
    }
    public static void main(String[] args) //test harness
    {
        Car car1 = new Car("123","car make",2);
        CustomerAccount acc1 = new CustomerAccount("Fname","Lname",car1,500);
        Car car2 = new Car("456","car make2",24);
        CustomerAccount acc2 = new CustomerAccount("Fname","Lname",car2,500);
        
        System.out.println(acc1.compareTo(acc2)); //should return -1
        System.out.println(acc1.toString());
    }
}