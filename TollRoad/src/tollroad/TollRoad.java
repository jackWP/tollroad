package tollroad;
import java.util.*;

/**
 * @author : Jack Warner-Pankhurst
 * Date : 23/02/18
 * Program : Toll road object class
 * Version : 3
 */

public class TollRoad {
    private HashMap<String, CustomerAccount> accountList; 
    //map to store accounts using regplate as unique hash key
    private int moneyMade; //keeps track of money made
    
    public TollRoad() //defualt constuctor
    {}
    public TollRoad(HashMap<String,CustomerAccount> accList, int money)
    {                                       //normal constuctor
        this.accountList = accList;
        this.moneyMade = money;
    }
    public HashMap<String, CustomerAccount> getCustomerAccounts()                                     
    {                                            //return account list as map
        return this.accountList;
    }
    public int getMoneyMade() //returns money made
    {
        return this.moneyMade;
    }
    public void addAccount(CustomerAccount acc) //adds account to map
    {
        this.accountList.put(acc.getVehicle().regPlate, acc);
    }
    public CustomerAccount findCustomer(String reg) 
            throws CustomerNotFoundException
            //adds funds account using regplate
    { 
        if (this.accountList.get(reg) != null) //if the search returns a value
        {
            return this.accountList.get(reg); //return found account
        }
        else 
        { //therefore account does not exist
            throw new CustomerNotFoundException();
        }
    } 
    
    public void chargeCustomer(CustomerAccount acc) 
    {//finds and charges customer, adds to money made
        try 
        {
            CustomerAccount acc2 = findCustomer(acc.getVehicle().getRegPlate());
            this.moneyMade += acc2.makeTrip(); //add trip cost to money made                 
        }
        catch (CustomerNotFoundException e)
        {
            System.out.println("Accouint does not exist. Continuing.");
        } //catches if customer not found from findCustomer
    }
    @Override
    public String toString()
    {
        return "Currently stored accounts: \n" + getCustomerAccounts();
    } //returns all currently stored accounts in string form
}
