package tollroad;

/**
 * @author : Jack Warner-Pankhurst
 * Date : 17/02/18
 * Program : Abstract Vehicle object class to be inherited by car, truck, van
 * Version : 1
 */
public abstract class Vehicle {
    protected final String regPlate; //registration plate 
    protected final String vehicleMake; //vehicle make
    public Vehicle() //default constructor
    {
        this.regPlate = "Not found.";
        this.vehicleMake = "Not Found.";
    }
    public Vehicle(String reg,String make) //normal constructor
    {
        this.regPlate = reg;
        this.vehicleMake = make;
    }
    public abstract int calculateBasicTrip(); 
    //method to be overriden by child classes
    
    public String getRegPlate() //get reg plate
    {
        return this.regPlate;
    }
    public String getVehicleMake() // get vehicle make
    {
        return this.vehicleMake;
    }
    @Override
    public String toString() //returns formatted string version of object
    {
        return this.regPlate + " - " + this.vehicleMake;
    }
} // No main as class is abstract

