package tollroad;

/**
 * @author : Jack Warner-Pankhurst
 * Date : 17/02/18
 * Program : Car object class, super = Vehicle
 * Version : 
 */
public class Car extends Vehicle {
    private final int noOfSeats; //Number of seats
    
    public Car() //defualt constructor
    {
        this.noOfSeats = -1; //to make errors more visable
    }
    public Car(String reg, String make,int seats) //normal constructor
    {
        super(reg,make);
        this.noOfSeats = seats;
    }
    public int getNoOfSeats() //gets no of seats
    {
        return this.noOfSeats;
    }

    @Override
    public int calculateBasicTrip() //override for Vehicle method (inherited)
    {
        if (noOfSeats <= 5)
        { //returns 500 if <=5 seats, otherwise 600
            return 500;
        }
        else return 600;
    }
    @Override
    public String toString()//returns formmatted string representation of object
    {
        return this.regPlate + " - " + this.vehicleMake +" - " + this.noOfSeats;
    }
    
    public static void main(String[] args) //Test harness
    {
        Car car = new Car("123abc","Ford",4); //test objects
        Car car2 = new Car("567qwe","Car2",7);
        System.out.println(car.calculateBasicTrip()); //test trips 
        System.out.println(car2.calculateBasicTrip());
        System.out.println(car.toString()); //test to string
    }
}
