package tollroad;

/**
 * @author : Jack Warner-Pankhurst
 * Date : 17/02/18
 * Program : Insufficient account balance exception class
 * Version : 1
 */
public class InsufficientAccountBalanceException extends java.lang.Exception{
    public InsufficientAccountBalanceException() //only defualt constuctor needed
    {                                   //error will be handled by program
        
    }
}
