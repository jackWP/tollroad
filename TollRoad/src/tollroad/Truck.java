package tollroad;

/**
 * @author : Jack Warner-Pankhurst
 * Date : 21/02/18
 * Program : Truck object class, super = vehicle
 * Version : 2
 */
public class Truck extends Vehicle {
    private final int noOfTrailers;
    
    public Truck() //Default constructor
    {
        this.noOfTrailers = -1; //to make errors more visable
    }
    public Truck(String reg, String make, int trailers) //normal constructor
    {
        super(reg,make);
        this.noOfTrailers = trailers;
    }
    public int getNoOfTrailers() //returns number of trailers
    {
        return this.noOfTrailers;
    }
    
    @Override
    public int calculateBasicTrip() //override for Vehicle method (inherited)
    {
        if(this.noOfTrailers == 1)
        {
            return 1250;
        }
        else
        {
            return 1500;
        }
    }
    @Override 
    public String toString() //returns formatted string representation of object
    {
        return this.regPlate + " - " + this.vehicleMake + " - " 
             + this.noOfTrailers;
    }
    public static void main(String[] args) //test harness
    {
        Truck truck = new Truck("123asd","Trucky",1);
        System.out.println(truck.calculateBasicTrip());
        System.out.println(truck.toString());
    }
}
