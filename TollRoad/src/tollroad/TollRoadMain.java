package tollroad;
import java.io.*;
import java.util.*;

public class TollRoadMain {
/**
 * @author : Jack Warner-Pankhurst
 * Date : 21/02/18
 * Program : Main class for program functionality
 * Version : 1
 */
    
    public static TollRoad initialiseTollRoadFromFile() throws IOException
    {
        HashMap<String,CustomerAccount> accountMap = new HashMap();
        //stores accounts using regplate as unique hash key
        
        File accounts = new File("customerData.txt"); //loads file
        Scanner scanLine =  new Scanner(accounts);
        scanLine.useDelimiter("#");
        //scanner to get lines in file
        while (scanLine.hasNext())
        { 
            String line = scanLine.next();           
            String[] details = line.split(","); 
                //splits line into words to build account and vehicle          
            Vehicle tempVehicle = buildVehicle(details[0],details[1],
                                  details[4],Integer.parseInt(details[5]));
            //builds vehicle
            CustomerAccount acc = new 
            CustomerAccount(details[2],details[3],tempVehicle,
            Integer.parseInt(details[6]));
            //builds account using built vehicle and other details
            switch(details[7])
            {
                case "STAFF" : acc.activateStaffDiscount();
                               break;
                case "FRIENDS_AND_FAMILY" : 
                               acc.activateFriends_And_FamilyDiscount();
                               break;
                default : break;
            }//activates discount, defaults to none as default discount is none
             //when account is initialised so no need to check for it
             
            accountMap.put(acc.getVehicle().regPlate, acc);
            //adds account to map using reg plate as key(always unique)
        }
        TollRoad road = new TollRoad(accountMap,0);//initialise new tollroad
        return road; //returns accounts
    }
    public static void simulateFromFile(TollRoad road) throws IOException
    {
        File transactions = new File("transactions.txt");//input file
        Scanner scanLine = new Scanner(transactions);
        scanLine.useDelimiter("\\$");//to split line
        
        while (scanLine.hasNext())//while there is a line
        {
            String line = scanLine.next();

            String[] details = line.split(",");//split ;ine into words
            try{
                switch(details[0])//first word is operation
                {
                    case "addFunds" : String reg1 = details[1];
                        road.findCustomer(reg1).addFunds
                        (Integer.parseInt(details[2]));
                        
                        WriteFile(reg1 
                                + " added funds successfully.");
                        System.out.println(reg1 
                                + " added funds successfully.");
                        break;//adds funds to account

                    case "makeTrip" :  String reg2 = details[1];                
                        CustomerAccount acc = road.findCustomer(reg2);
                        road.chargeCustomer(acc);
                        
                        WriteFile(reg2 + " made trip successfully.");
                        System.out.println(reg2 + " made trip successfully.");
                        break;//makes trip on found account
                }
            }
            catch (CustomerNotFoundException e)
            {
                WriteFile("Account does not exist. Continuing.");
                System.out.println("Account does not exist. Continuing.");
            }//catch customer not found from findCustomer
        }
        WriteFile("Total money made: " + road.getMoneyMade());
        System.out.println("Total money made: " + road.getMoneyMade());
        //print total money made
    }
    
    public static Vehicle buildVehicle(String type, String reg, String make, int detail)
    {//builds vehicle based off input string for type
        switch (type)
        {
            case "Car" : Car car =  new Car(reg,make,detail);
                         return car;
            case "Van" : Van van = new Van(reg,make,detail);
                         return van;
            case "Truck" : Truck truck = new Truck(reg,make,detail);
                           return truck;
        }
        return null; //NEVER HAPPENS
    }
    
    public static void WriteFile(String text)
    {
        try 
        {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("output.txt", true)));
            out.println(text);
            out.close();
        } 
        catch (IOException e) 
        {
            System.out.println(e);
        }
    }
    
    public static void main(String[] args) {
        try {
            simulateFromFile(initialiseTollRoadFromFile());
        } catch (IOException ex) {//for IO errors
            System.out.println(ex);
        }
    }  
}