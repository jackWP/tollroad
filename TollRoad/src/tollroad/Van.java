package tollroad;

/**
 * @author : Jack Warner-Pankhurst
 * Date : 17/02/18
 * Program : vehicle object class, super = vehicle
 * Version : 1
 */
public class Van extends Vehicle {
    private final int payload; //Paylaod
    
    public Van() //defualt constructor
    {
        this.payload = -1; //to make errors more visable
    }
    public Van(String reg, String make,int load) //normal constructor
    {
        super(reg,make);
        this.payload = load;
    }
    public int getPayload() //gets paylaod
    {
        return this.payload;
    }
    @Override
    public int calculateBasicTrip() //override for Vehicle method (inherited)
    {//if payload is <600, return 500, if between 800 and 600, return 750
     //else return 1000 as paylaod must be greater than 800
        if (this.payload <= 600)
        { 
            return 500;
        }
        else if (this.payload <= 800 && this.payload >600 )
        {
            return 750;
        }
        else return 1000;
    }
    @Override
    public String toString() //returns formatted string representation of object
    {
        return this.regPlate + " - " + this.vehicleMake + " - " + this.payload;
    }
    public static void main(String[] args) //test harness
    {
        Van van = new Van("abd","Vanny",500);
        System.out.println(van.calculateBasicTrip());
        System.out.println(van.toString());
    }
    
}
