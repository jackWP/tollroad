package tollroad;

/**
 * @author : Jack Warner-Pankhurst
 * Date : 17/02/18
 * Program : Customer account not found exception class
 * Version : 1
 */
public class CustomerNotFoundException extends java.lang.Exception {
    public CustomerNotFoundException() //only defualt constuctor needed
    {                                  //error will be handled by program
        
    }
}
